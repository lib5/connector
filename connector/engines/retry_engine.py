from typing import Type, Optional, Callable

from requests import Response
from tenacity import Retrying

from connector.configuration.retry_engine_configuration import RetryEngineConfig
from connector.engines.exceptions import RetryingInstanceNotSetError


class _RetryEngine:
    def __init__(self, retrying_class: Type[Retrying]):
        self._retrying_class = retrying_class
        self._retrying: Optional[Retrying] = None
        self._config: Optional[RetryEngineConfig] = None

    def configure(self, config: Optional[RetryEngineConfig]):
        if config:
            self._retrying = self._retrying_class(**config.dict())
            self._config = config

    def retry(self, fn: Callable, *args, **kwargs) -> Response:
        try:
            return self._retrying(fn=fn, *args, **kwargs)
        except TypeError:
            raise RetryingInstanceNotSetError('retrying instance is not set')

    def __call__(self, config: Optional[RetryEngineConfig] = None) -> '_RetryEngine':
        self.configure(config=config)
        return self


RetryEngine = _RetryEngine(retrying_class=Retrying)
