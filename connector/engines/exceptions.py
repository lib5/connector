class EnginesBaseError(Exception):
    def __str__(self):
        return self.args[0]


class RetryingInstanceNotSetError(EnginesBaseError):
    pass
