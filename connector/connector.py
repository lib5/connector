from typing import Optional

from requests import Response

from connector.configuration import HTTPSessionConfig, RetryEngineConfig
from connector.engines import RetryEngine
from connector.transports import HttpTransport


class _Connector:
    def __init__(self, http_transport: HttpTransport, retry_engine: RetryEngine):
        self._http_transport_callable = http_transport
        self._retry_engine_callable = retry_engine
        self._transport: Optional[HttpTransport] = None
        self._retry_engine: Optional[RetryEngine] = None

    def send(self, http_method: Optional[str] = 'get', url: Optional[str] = None, **kwargs) -> Response:
        return self._retry_engine.retry(fn=self._transport.send, http_method=http_method, url=url, **kwargs)

    def __call__(self, http_session_config: HTTPSessionConfig, retry_engine_config: RetryEngineConfig) -> '_Connector':
        self._transport = self._http_transport_callable(session_config=http_session_config)
        self._retry_engine = self._retry_engine_callable(config=retry_engine_config)
        return self


Connector = _Connector(http_transport=HttpTransport, retry_engine=RetryEngine)
