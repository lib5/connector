class TransportsBaseError(Exception):
    def __str__(self):
        return self.args[0]


class NoSessionInstanceError(TransportsBaseError):
    pass


class NoSessionConfigError(TransportsBaseError):
    pass


class InvalidSessionConfigurationAttributeError(TransportsBaseError):
    pass


class InvalidURLError(TransportsBaseError):
    pass


class InvalidHTTPMethodError(TransportsBaseError):
    pass


class HTTPTransportSendError(TransportsBaseError):
    pass
