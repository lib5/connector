from functools import singledispatchmethod
from typing import Type, Optional, Callable, Union
from urllib.parse import urlparse, urljoin, ParseResult

from requests import Session, Response

from connector.transports.exceptions import (
    NoSessionInstanceError,
    InvalidURLError,
    NoSessionConfigError,
    InvalidHTTPMethodError,
    HTTPTransportSendError
)
from connector.configuration import HTTPSessionConfig


class _HttpTransport:
    def __init__(
            self,
            session_class: Type[Session],
            parse_url_fn: Callable[[str], ParseResult],
            join_url_fn: Callable[[str, str, Optional[bool]], str]
    ):
        self._session_class = session_class
        self._parse_url = parse_url_fn
        self._join_url = join_url_fn
        self._session: Optional[Session] = None
        self._session_config: Optional[HTTPSessionConfig] = None
        self._base_url: Optional[str] = None

    @staticmethod
    def _clean_url_or_path(url_or_path: Optional[str] = None) -> Union[str, None]:
        if url_or_path:
            return url_or_path.strip().lstrip('/').rstrip('/').strip()

    @property
    def session_config(self) -> HTTPSessionConfig:
        return self._session_config

    @session_config.setter
    def session_config(self, session_config: HTTPSessionConfig):
        self._session_config = session_config

    def _create_session(self):
        self._session = self._session_class()

    def _check_session_is_set(self):
        if self._session is None:
            raise NoSessionInstanceError('session is not set')

    def _check_session_config_is_set(self):
        if self._session_config is None:
            raise NoSessionConfigError('session config is not set')

    @singledispatchmethod
    def _set_session_attribute(self, attr_value, attr_name: str):
        setattr(self._session, attr_name, attr_value)

    @_set_session_attribute.register
    def _(self, attr_value: dict, attr_name: str):
        getattr(self._session, attr_name).update(attr_value)

    def _is_url(self, url_or_path: str) -> bool:
        parsed_url = self._parse_url(url_or_path)
        return parsed_url.scheme in ('http', 'https')

    def _build_url(self, url_or_path: Optional[str] = None) -> str:
        url_or_path = self._clean_url_or_path(url_or_path=url_or_path)
        if self._is_url(url_or_path=url_or_path):
            return url_or_path
        if self._base_url is None:
            raise InvalidURLError(f'url {url_or_path or ""} is not a valid URL and base url is not set')
        return self._join_url(self._base_url, url_or_path) # noqa allow_fragments is optional

    @staticmethod
    def _clean_http_method(http_method: str) -> str:
        return http_method.strip().lower()

    def _get_http_method_from_session(self, http_method: str) -> Callable:
        method = self._clean_http_method(http_method=http_method)
        try:
            return getattr(self._session, method)
        except AttributeError:
            raise InvalidHTTPMethodError(f'{http_method} is not a valid HTTP method')

    def configure_session(self):
        self._check_session_is_set()
        self._check_session_config_is_set()
        for k, v in self._session_config.dict().items():
            self._set_session_attribute(v, attr_name=k)

    def send(self, http_method: str, url: Optional[str] = None, **kwargs) -> Response:
        self._check_session_is_set()
        method = self._get_http_method_from_session(http_method=http_method)
        url = self._build_url(url_or_path=url)
        try:
            return method(url, **kwargs)
        except Exception as e:
            raise HTTPTransportSendError('send method failed') from e

    def __call__(self, session_config: Optional[HTTPSessionConfig] = None) -> '_HttpTransport':
        self._create_session()
        if session_config:
            self.session_config = session_config
            self.configure_session()
            self._base_url = self._clean_url_or_path(url_or_path=session_config.base_url)
        return self


HttpTransport = _HttpTransport(session_class=Session, parse_url_fn=urlparse, join_url_fn=urljoin)
