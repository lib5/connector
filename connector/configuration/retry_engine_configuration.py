from typing import Optional, Dict, Callable, Any

import tenacity
from tenacity import stop_any


class RetryConfigAttribute:
    def __set_name__(self, owner, name):
        self.name = name

    def __get__(self, instance, owner):
        if instance:
            return instance.__dict__.get(self.name)
        return owner.__dict__.get(self.name)

    def __set__(self, instance, value):
        if value:
            predicate_fn = getattr(tenacity, self.name)
            instance.__dict__[self.name] = predicate_fn(value)


class RetryEngineConfig:
    stop_after_attempt = RetryConfigAttribute()
    stop_after_delay = RetryConfigAttribute()

    def __init__(
            self,
            stop_after_attempt: Optional[int] = 1,
            stop_after_delay: Optional[float] = None
    ):
        self.stop_after_attempt = stop_after_attempt
        self.stop_after_delay = stop_after_delay

    def _get_not_none_attrs(self, group: Optional[str] = None) -> Dict[str, Callable]:
        if group:
            return {attr[0]: attr[1] for attr in self.__dict__.items() if attr[0].startswith(group)}
        return self.__dict__

    @property
    def _stop_predicate_fns(self) -> Dict[str, stop_any]:
        not_none_attrs = self._get_not_none_attrs(group='stop')
        if any(not_none_attrs):
            return {'stop': stop_any(*not_none_attrs.values())}
        return {}

    def dict(self) -> Dict[str, Callable[[Any], bool]]:
        return {**self._stop_predicate_fns}
