from typing import Optional, Dict, Any, Union, Tuple, Callable, List

from pydantic import BaseModel, HttpUrl
from requests import Request, Response
from requests.auth import AuthBase
from requests.cookies import RequestsCookieJar


# Requests types
Headers = Dict[str, str]
Auth = Union[Tuple[str, str], AuthBase, Callable[[Request], Request]]
Proxies = Dict[str, str]
Hook = Callable[[Response], Any]
Hooks = Dict[str, List[Hook]]
Params = Union[Dict[str, str], List[Tuple[str, str]]]
Verify = Union[bool, str]
Cert = Union[str, Tuple[str, str]]
Cookies = Union[RequestsCookieJar, dict]


class HTTPSessionConfig(BaseModel):
    class Config:
        arbitrary_types_allowed = True

    base_url: Optional[HttpUrl] = None
    headers: Optional[Headers] = None
    auth: Optional[Auth] = None
    proxies: Optional[Proxies] = None
    hooks: Optional[Hooks] = None
    params: Optional[Params] = None
    stream: Optional[bool] = None
    verify: Optional[Verify] = None
    cert: Optional[Cert] = None
    max_redirects: Optional[int] = None
    trust_env: Optional[bool] = None
    cookies: Optional[Cookies] = None

    def dict(self, *args, **kwargs) -> Dict[str, Any]:
        return super().dict(exclude_none=True, exclude={'base_url'}, *args, **kwargs)
