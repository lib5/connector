import responses as r

from connector.configuration import HTTPSessionConfig, RetryEngineConfig
from connector.connector import Connector


MOCK_URL = 'https://mock-url.com'


@r.activate
def test_empty_configuration_makes_http_request():
    r.add(r.GET, MOCK_URL, status=200)
    sc = HTTPSessionConfig()
    rec = RetryEngineConfig()
    c = Connector(http_session_config=sc, retry_engine_config=rec)
    response = c.send(url=MOCK_URL)
    assert response.status_code == 200
