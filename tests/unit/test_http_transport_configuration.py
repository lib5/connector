import pytest

from pydantic.error_wrappers import ValidationError

from connector.configuration import HTTPSessionConfig


def test_dict_returns_filled_fields():
    instance = HTTPSessionConfig(headers={})
    assert instance.dict() == {'headers': {}}


def test_dict_excludes_none():
    instance = HTTPSessionConfig()
    assert instance.dict() == {}
    instance = HTTPSessionConfig(stream=True)
    assert len(instance.dict()) == 1


def test_dict_excludes_base_url():
    instance = HTTPSessionConfig(base_url='https://test.com') # noqa HttpUrl inherits from str
    assert instance.dict() == {}


def test_http_session_config_validates_type():
    with pytest.raises(ValidationError):
        HTTPSessionConfig(trust_env='') # noqa
