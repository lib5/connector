from unittest.mock import Mock

import pytest
from tenacity import Retrying

from connector.configuration.retry_engine_configuration import RetryEngineConfig
from connector.engines.exceptions import RetryingInstanceNotSetError
from connector.engines.retry_engine import _RetryEngine


def test_init_no_params():
    instance = _RetryEngine(retrying_class=Retrying)
    assert instance._retrying_class is Retrying
    assert instance._retrying is None
    assert instance._config is None


def test_configure_no_config_does_nothing():
    instance = _RetryEngine(retrying_class=Retrying)
    assert instance._retrying is None
    assert instance._config is None


def test_configure_with_config_sets_self_retrying_and_config():
    instance = _RetryEngine(retrying_class=Retrying)
    rec = RetryEngineConfig()
    instance.configure(config=rec)
    assert isinstance(instance._retrying, Retrying)
    assert instance._config is rec


def test_configure_with_config_sets_retry_functions_in_retrying():
    instance = _RetryEngine(retrying_class=Retrying)
    rec = RetryEngineConfig(stop_after_attempt=1)
    instance.configure(config=rec)
    assert instance._retrying.stop.stops[0].max_attempt_number == 1


def test_retry_method_retries_passed_function():
    fn_to_retry = Mock()
    fn_to_retry.side_effect = [Exception(), None]
    instance = _RetryEngine(retrying_class=Retrying)
    rec = RetryEngineConfig(stop_after_attempt=2)
    instance.configure(config=rec)
    instance.retry(fn=fn_to_retry)
    assert fn_to_retry.call_count == 2


def test_retry_method_raises_when_retry_instance_is_not_set():
    instance = _RetryEngine(retrying_class=Retrying)
    fn_to_retry = Mock()
    with pytest.raises(RetryingInstanceNotSetError) as ex_info:
        instance.retry(fn=fn_to_retry)
    assert str(ex_info.value) == 'retrying instance is not set'


def test_call_with_no_params_leaves_retry_and_config_as_none():
    instance = _RetryEngine(retrying_class=Retrying)
    instance()
    assert instance._retrying is None
    assert instance._config is None


def test_call_with_params_configures_and_returns_self():
    instance = _RetryEngine(retrying_class=Retrying)
    rec = RetryEngineConfig()
    instance(config=rec)
    assert isinstance(instance._retrying, Retrying)
    assert isinstance(instance._config, RetryEngineConfig)
    assert isinstance(instance, _RetryEngine)
