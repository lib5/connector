import tenacity
from tenacity import stop_any

from connector.configuration.retry_engine_configuration import RetryEngineConfig, RetryConfigAttribute


def test_class_attr_is_descriptor():
    assert isinstance(RetryEngineConfig.stop_after_attempt, RetryConfigAttribute)


def test_init_no_params():
    instance = RetryEngineConfig()
    assert instance.stop_after_attempt is None


def test_init_with_params_instantiate_tenacity_classes():
    instance = RetryEngineConfig(stop_after_attempt=1, stop_after_delay=2)
    for attr_name, attr_value in instance.__dict__.items():
        assert isinstance(attr_value, getattr(tenacity, attr_name))


def test_get_not_none_attrs_no_params():
    instance = RetryEngineConfig()
    assert len(instance._get_not_none_attrs()) == 0


def test_get_not_none_attrs_with_params_no_group():
    instance = RetryEngineConfig(stop_after_attempt=1)
    assert len(instance._get_not_none_attrs()) == 1


def test_get_not_none_attrs_with_params_and_group():
    instance = RetryEngineConfig(stop_after_attempt=1)
    assert len(instance._get_not_none_attrs(group='stop')) == 1


def test_stop_predicate_fns_returns_empty_dict_when_attrs_are_none():
    instance = RetryEngineConfig()
    assert isinstance(instance._stop_predicate_fns, dict)
    assert len(instance._stop_predicate_fns) == 0


def test_stop_predicate_fns_returns_dict_with_stop_any_as_values_when_attrs_are_not_none():
    instance = RetryEngineConfig(stop_after_attempt=1)
    assert len(instance._stop_predicate_fns) == 1
    for _, v in instance._stop_predicate_fns.items():
        assert isinstance(v, stop_any)


def test_dict_returns_str_callable_dict():
    instance = RetryEngineConfig(stop_after_attempt=1)
    for k, v in instance.dict().items():
        assert isinstance(k, str)
        assert callable(v)
