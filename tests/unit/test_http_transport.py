from contextlib import contextmanager
from typing import Optional
from unittest.mock import Mock
from urllib.parse import urlparse, urljoin, ParseResult

import pytest
from requests import Session, Response

from connector.configuration import HTTPSessionConfig
from connector.transports.exceptions import (
    NoSessionInstanceError,
    InvalidURLError,
    NoSessionConfigError,
    InvalidHTTPMethodError,
    HTTPTransportSendError
)
from connector.transports.http_transport import _HttpTransport


class FakeSession(Session):
    pass


def fake_parse_url_fn(url: str) -> ParseResult:
    pass


def fake_join_url_fn(base: str, url: str, allow_fragments: Optional[bool] = True) -> str:
    pass


@pytest.fixture
def http_transport_instance():
    return _HttpTransport(session_class=FakeSession, parse_url_fn=fake_parse_url_fn, join_url_fn=fake_join_url_fn)


@pytest.fixture
def instance_with_url_fns(http_transport_instance: _HttpTransport):
    http_transport_instance._parse_url = urlparse
    http_transport_instance._join_url = urljoin
    return http_transport_instance


testing_urls_and_paths = [
    ('http://test.com', 'http://test.com'),
    ('http://test.com/', 'http://test.com'),
    ('/http://test.com/', 'http://test.com'),
    ('http://test.com /', 'http://test.com'),
    ('/item', 'item'),
    ('item/', 'item'),
    ('/item/', 'item'),
    (' /item/', 'item'),
    (' / item / ', 'item'),
    (None, None),
]

http_str_methods = [
    ('GET', 'get'),
    (' GET', 'get'),
    (' Get', 'get'),
    (' Get ', 'get'),
    ('get', 'get'),
    ('', ''),
]

fake_session_instance = FakeSession()
requests_session_http_methods = [
    ('get', fake_session_instance.get),
    ('post', fake_session_instance.post),
    ('put', fake_session_instance.put),
    ('delete', fake_session_instance.delete),
    ('patch', fake_session_instance.patch),
    ('options', fake_session_instance.options),
    ('head', fake_session_instance.head),
]


@contextmanager
def does_not_raise():
    yield


send_url_args = [
    ('get', 'http://example.com', does_not_raise()),
    ('get', 'https://example.com', does_not_raise()),
    ('get', 'path', pytest.raises(InvalidURLError)),
    ('get', None, pytest.raises(InvalidURLError)),
]


def test_http_transport_init(http_transport_instance: _HttpTransport):
    assert issubclass(http_transport_instance._session_class, Session)
    assert http_transport_instance._parse_url is fake_parse_url_fn
    assert http_transport_instance._join_url is fake_join_url_fn
    assert http_transport_instance._session is None
    assert http_transport_instance._session_config is None
    assert http_transport_instance._base_url is None


@pytest.mark.parametrize('test_input, expected', testing_urls_and_paths)
def test_clean_url_or_path(test_input, expected, http_transport_instance: _HttpTransport):
    assert http_transport_instance._clean_url_or_path(url_or_path=test_input) == expected


def test_session_config_property(http_transport_instance: _HttpTransport):
    assert http_transport_instance.session_config is None
    http_transport_instance.session_config = HTTPSessionConfig()
    assert isinstance(http_transport_instance.session_config, HTTPSessionConfig)


def test_create_session(http_transport_instance: _HttpTransport):
    http_transport_instance._create_session()
    assert isinstance(http_transport_instance._session, Session)


def test_check_session_is_set(http_transport_instance: _HttpTransport):
    with pytest.raises(NoSessionInstanceError) as ex_info:
        http_transport_instance._check_session_is_set()
    assert str(ex_info.value) == 'session is not set'


def test_check_session_config_is_set(http_transport_instance: _HttpTransport):
    with pytest.raises(NoSessionConfigError) as ex_info:
        http_transport_instance._check_session_config_is_set()
    assert str(ex_info.value) == 'session config is not set'


def test_set_session_attribute_no_dict(http_transport_instance: _HttpTransport):
    fake_session = FakeSession()
    fake_session.fake_attr = True
    http_transport_instance._session = fake_session
    assert http_transport_instance._session.fake_attr is True # noqa
    http_transport_instance._set_session_attribute(False, 'fake_attr')
    assert http_transport_instance._session.fake_attr is False # noqa


def test_set_session_attribute_dict(http_transport_instance: _HttpTransport):
    fake_session = FakeSession()
    original_headers = fake_session.headers
    new_headers = {'custom_header': 'value'}
    http_transport_instance._session = fake_session
    http_transport_instance._(new_headers, 'headers')
    assert http_transport_instance._session.headers == {**original_headers, **new_headers}


def test_is_url_false(http_transport_instance: _HttpTransport):
    parse_result_false = Mock(scheme='')
    http_transport_instance._parse_url = lambda _: parse_result_false
    assert http_transport_instance._is_url(url_or_path='') is False


def test_is_url_true(http_transport_instance: _HttpTransport):
    parse_result_true = Mock(scheme='http')
    http_transport_instance._parse_url = lambda _: parse_result_true
    assert http_transport_instance._is_url(url_or_path='') is True


def test_build_url_base_url_returns_same_url_when_url_is_passed(instance_with_url_fns: _HttpTransport):
    url = 'https://google.com'
    assert instance_with_url_fns._build_url(url_or_path=url) == url


def test_build_url_base_url_raises_when_invalid_url_is_passed_and_no_base_set(instance_with_url_fns: _HttpTransport):
    url = ''
    with pytest.raises(InvalidURLError) as ex_info:
        instance_with_url_fns._build_url(url_or_path=url)
    assert str(ex_info.value) == f'url {url} is not a valid URL and base url is not set'


def test_build_url_base_url_returns_valid_url_when_base_set(instance_with_url_fns: _HttpTransport):
    base_url = 'https://test.com'
    path = 'item'
    instance_with_url_fns._base_url = base_url
    assert instance_with_url_fns._build_url(url_or_path=path) == f'{base_url}/{path}'


@pytest.mark.parametrize('test_input, expected', http_str_methods)
def test_clean_http_method_returns_cleaned_str(test_input, expected, http_transport_instance: _HttpTransport):
    assert http_transport_instance._clean_http_method(http_method=test_input) == expected


@pytest.mark.parametrize('test_input, expected', requests_session_http_methods)
def test_get_http_method_from_session_returns_session_instance_methods(
        test_input,
        expected,
        http_transport_instance: _HttpTransport
):
    http_transport_instance._session = fake_session_instance
    result = http_transport_instance._get_http_method_from_session(http_method=test_input)
    # Test the returned methods belong to the same session instance
    # and have the expected name.
    assert result.__self__ == fake_session_instance # noqa
    assert result.__name__ == expected.__name__


def test_get_http_method_from_session_raises_when_method_is_invalid(http_transport_instance: _HttpTransport):
    invalid_method = 'xxx'
    http_transport_called = http_transport_instance()
    with pytest.raises(InvalidHTTPMethodError) as ex_info:
        http_transport_called._get_http_method_from_session(http_method=invalid_method)
    assert str(ex_info.value) == f'{invalid_method} is not a valid HTTP method'


def test_configure_session_raises_when_no_session(http_transport_instance: _HttpTransport):
    with pytest.raises(NoSessionInstanceError) as ex_info:
        http_transport_instance.configure_session()
    assert str(ex_info.value) == 'session is not set'


def test_configure_session_raises_when_no_session_config(http_transport_instance: _HttpTransport):
    http_transport_instance._create_session()
    with pytest.raises(NoSessionConfigError) as ex_info:
        http_transport_instance.configure_session()
    assert str(ex_info.value) == 'session config is not set'


def test_configure_session_sets_session_attributes(http_transport_instance: _HttpTransport):
    http_transport_instance._create_session()
    custom_headers = {'key': 'value'}
    custom_auth = ('auth', 'value')
    custom_proxies = {'http': 'foo.bar:3128'}
    custom_params = {'key': 'value'}
    custom_stream = True
    custom_verify = False
    custom_cert = 'path/to/cert.pem'
    custom_trust_env = False
    custom_max_redirects = 10
    custom_cookies = {'key': 'value'}

    config = HTTPSessionConfig(
        headers=custom_headers,
        auth=custom_auth,
        proxies=custom_proxies,
        params=custom_params,
        stream=custom_stream,
        verify=custom_verify,
        cert=custom_cert,
        max_redirects=custom_max_redirects,
        trust_env=custom_trust_env,
        cookies=custom_cookies
    )

    http_transport_instance.session_config = config
    http_transport_instance.configure_session()

    assert set(http_transport_instance._session.headers) > set(custom_headers)
    assert http_transport_instance._session.auth == custom_auth
    assert http_transport_instance._session.proxies == custom_proxies
    assert http_transport_instance._session.params == custom_params
    assert http_transport_instance._session.stream == custom_stream
    assert http_transport_instance._session.verify == custom_verify
    assert http_transport_instance._session.cert == custom_cert
    assert http_transport_instance._session.trust_env == custom_trust_env
    assert http_transport_instance._session.max_redirects == custom_max_redirects
    assert http_transport_instance._session.cookies == custom_cookies


def test_send_returns_session_method_result(instance_with_url_fns: _HttpTransport):
    r = Response()
    s = Mock(spec=Session)
    s.get.return_value = r
    instance_with_url_fns._session = s
    assert instance_with_url_fns.send(http_method='GET', url='http://test.com') is r


def test_send_raises_when_no_session_set(instance_with_url_fns: _HttpTransport):
    with pytest.raises(NoSessionInstanceError):
        instance_with_url_fns.send(http_method='GET', url='http://test.com')


@pytest.mark.parametrize('method, url, expectation', send_url_args)
def test_send_raises_when_called_with_no_url_and_no_base_url_is_set(
        method,
        url,
        expectation,
        instance_with_url_fns: _HttpTransport
):
    s = Mock(spec=Session)
    instance_with_url_fns._session = s
    with expectation:
        assert instance_with_url_fns.send(http_method=method, url=url)


def test_send_raises_custom_exception_when_session_method_call_error(instance_with_url_fns: _HttpTransport):
    class FakeSessionException(Exception):
        pass
    s = Mock(spec=Session)
    s.get.side_effect = FakeSessionException()
    instance_with_url_fns._session = s
    with pytest.raises(HTTPTransportSendError) as ex_info:
        instance_with_url_fns.send('get', 'http://example.com')
    # Check raises custom exception from original exception
    assert isinstance(ex_info.value.__cause__, FakeSessionException)


def test_call_no_session_config(instance_with_url_fns: _HttpTransport):
    called = instance_with_url_fns()
    assert isinstance(called, _HttpTransport)
    assert isinstance(called._session, FakeSession)


def test_call_with_session_config(instance_with_url_fns: _HttpTransport):
    base_url = 'https://www.test.com'
    headers = {'key': 'value'}
    hsc = HTTPSessionConfig(base_url=base_url, headers=headers) # noqa
    called = instance_with_url_fns(session_config=hsc)
    assert called.session_config is hsc
    assert called._base_url == base_url
    assert set(called._session.headers) > set(headers)
