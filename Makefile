pytest := pytest -vs
unit := tests/unit
transport := test_http_transport.py
retry_config := test_retry_engine_configuration.py

test_unit:
	${pytest} ${unit}

test_unit_transport:
	${pytest} ${unit}/${transport}::${test}

test_retry_config:
	${pytest} -k ${unit}/${retry_config}::${test}